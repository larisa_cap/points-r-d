package points.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import points.service.PointService;
import points.model.Point;

import java.util.List;

@Controller
public class PointsController {
    @Autowired
    PointService pointService;

    @RequestMapping(value = {"/list"}, method = RequestMethod.GET)
    public @ResponseBody List<Point> points() {
        return pointService.list();
    }

    @RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
    public @ResponseBody Point show(@PathVariable("id") long id) {
        return pointService.get(id);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody Point add(Point point) {
        pointService.add(point);
        return point;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") long pointId) {
        pointService.delete(pointId);
    }

    @RequestMapping(value = "/deleteall", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteAll() {
        pointService.deleteAll();
    }
}
