package points.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import points.model.Point;
import points.repository.PointDao;

import java.util.List;

@Service
public class PointService {
    @Autowired
    PointDao pointDao;

    public List<Point> list() {
        return pointDao.list();
    }

    public Point get(long id) {
        return pointDao.get(id);
    }

    public void add(Point point) {
        pointDao.add(point);
    }

    public void delete(long pointId) {
        pointDao.delete(pointId);
    }

    public void deleteAll() {
        pointDao.deleteAll();
    }
}
