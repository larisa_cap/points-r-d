var pointsControllers = angular.module('pointsControllers', []);

pointsControllers.controller('PointShowController', ['$scope', '$routeParams', '$http', '$location',
    function($scope, $routeParams, $http, $location) {
        $scope.pointId = $routeParams.pointId;
        $http.get('../main/show/' + $scope.pointId).success(function(data) {
            $scope.point = data;
            new Map([$scope.point]);
        });

        $scope.deletePoint = function() {
            $http.delete('../main/delete/' + $scope.pointId).success(function() {
                $location.path('/points');
            });
        }
    }
]);

pointsControllers.controller('PointsListController', ['$scope', '$http', '$route',
    function ($scope, $http, $route) {
        $http.get('../main/list').success(function(data) {
            $scope.points = data;
            $scope.map = new Map($scope.points);
        });

        $scope.deleteAllPoints = function() {
            $http.delete('../main/deleteall').success(function() {
                $route.reload();
            });
        }
    }
]);

pointsControllers.controller('PointsAddController', ['$scope', '$http',
    function($scope, $http) {
        $scope.point = {};

        $scope.createPoint = function() {
            var form = $('#add_point_form');
            var name = $('.name-group input', form).val();
            $('.name-group .error_list').empty();
            $('.address-group .error_list').empty();
            if (name == "") {
                $('#errorTmpl').tmpl([{error: "Введите, пожалуйста, название точки"}]).appendTo('.name-group .error_list');
            }
            var address = $('.address-group input', form).val();
            getAddress(address, {
                success: function(location) {
                    $scope.point.address = location.formatted_address;
                    $scope.point.lat = location.geometry.location.lat;
                    $scope.point.lon = location.geometry.location.lng;
                    $http.post('../main/add', $.param($scope.point), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function(point) {
                        $scope.points.push(point);
                        $scope.map.addMarkers([point])
                    });
                },
                fail: function() {
                    $('#errorTmpl').tmpl([{error: "Введите, пожалуйста, корректный адрес"}]).appendTo('.address-group .error_list');
                }
            });
        };
    }
]);
