function getAddress(address, callbacks) {
    $.ajax({
        url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false',
        success: function(data) {
            if (data.status == 'OK') {
                if (typeof callbacks.success == 'function')
                    callbacks.success(data.results[0]);
            } else {
                if (typeof callbacks.fail == 'function')
                    callbacks.fail(data.status);
            }
        },
        dataType: 'json'
    });
}
