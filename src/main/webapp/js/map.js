function Map(points) {
    var self = this;
    init();

    function init() {
        self.geocoder = new google.maps.Geocoder();
        self.markers = [];
        self.map = createMap();
        self.addMarkers(points);
    }

    function createMap() {
        var myOptions = {
            zoom: 8,
            center: new google.maps.LatLng(55.7522200, 37.6155600),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        return new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    }
}

Map.prototype.addMarkers = function(points) {
    for (var i in points) {
        var point = points[i];
        this.markers.push(new google.maps.Marker({
            position: new google.maps.LatLng(point.lat, point.lon),
            map: this.map,
            title: point.name
        }));
    }
    if (points.length > 0) {
        this.zoomMap();
    }
};

Map.prototype.zoomMap = function() {
    var self = this;
    var bounds = new google.maps.LatLngBounds();
    for (var i in this.markers) {
        bounds.extend(this.markers[i].position);
    }
    self.map.fitBounds(bounds);
    var listener = google.maps.event.addListener(this.map, "idle", function() {
        if (self.map.getZoom() > 16) self.map.setZoom(16);
        google.maps.event.removeListener(listener);
    });
};